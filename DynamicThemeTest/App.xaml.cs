﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Xml.Linq;

namespace DynamicThemeTest
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            //LoadTheme();
        }

        private void LoadTheme()
        {
            ResourceDictionary dict = new ResourceDictionary();
            var doc = XDocument.Load("../../Red_Theme.xml");
            var theme = doc.Element("palette");
            foreach (var colorset in theme.Descendants("colorset"))
            {
                foreach (var color in colorset.Descendants("color"))
                {
                    dict.Add(color.Attribute("id").Value.Replace('-','_').ToUpper(), (Color)ColorConverter.ConvertFromString("#" + color.Attribute("rgb").Value));
                }
            }
            Current.Resources.MergedDictionaries.Add(dict);
            dict = new ResourceDictionary()
            {
                Source = new Uri("Styles.xaml", UriKind.Relative)
            };
            Current.Resources.MergedDictionaries.Add(dict);
        }
    }
}
