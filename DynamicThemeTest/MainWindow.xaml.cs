﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;

namespace DynamicThemeTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //var doc = XDocument.Load("../../DefaultTheme.xml");
            //SetTheme(doc);
            SetThemeXaml(new Uri("pack://application:,,,/DefaultTheme.xaml", UriKind.RelativeOrAbsolute));
        }

        private void SetTheme(XDocument doc)
        {
            ResourceDictionary dict = new ResourceDictionary();
            var theme = doc.Element("palette");
            foreach (var colorset in theme.Descendants("colorset"))
            {
                foreach (var color in colorset.Descendants("color"))
                {
                    dict.Add(color.Attribute("id").Value.Replace('-', '_').ToUpper(), (Color)ColorConverter.ConvertFromString("#" + color.Attribute("rgb").Value));
                }
            }
            
            Application.Current.Resources.MergedDictionaries.Add(dict);
        }

        private void SetThemeXaml(Uri path)
        {
            ResourceDictionary dict = new ResourceDictionary() { Source = path};
            Application.Current.Resources.MergedDictionaries.Add(dict);
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //var doc = XDocument.Load("../../Red_Theme.xml");
            //SetTheme(doc);
            SetThemeXaml(new Uri("pack://application:,,,/Red_Theme.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
